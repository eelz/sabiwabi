FROM       debian:latest
RUN        apt-get update
RUN        apt-get dist-upgrade -y
RUN        apt-get install -y python3 python3-pip git
RUN        pip3 install ipython matplotlib pytest
ARG        userID
ARG        userName
RUN        useradd -m -d /home/$userName -U -u $userID $userName
USER       $userName
